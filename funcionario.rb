require './departamento.rb'
require './dependente.rb'

class Funcionario
    attr_accessor :primeiro_nome, :segundo_nome, :idade, :salario, :endereco, 
    :dependentes, :depto, :matricula

    def initialize params
        @primeiro_nome = params[:primeiro_nome]
        @segundo_nome = params[:segundo_nome]
        @idade = params[:idade]
        @salario = params[:salario]
        @endereco = params[:endereco]
        @dependentes = Array.new
        @matricula = params[:matricula]
    end

    def nome_completo
        "#{self.primeiro_nome} #{self.segundo_nome}"
    end

    def add_dependente (dependente:)
        self.dependentes.append(dependente)
        return nil
    end

    def num_dependentes 
        self.dependentes.length
    end

end