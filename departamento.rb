require './funcionario.rb'

class Departamento
    attr_accessor :nome, :gerente, :funcionarios

    def initialize (nome:)
        @funcionarios = Array.new
        @nome = nome
    end

    def add_funcionario (funcionario:)
        self.funcionarios.append(funcionario)
        funcionario.depto = self
        return nil
    end

    def calc_gastos 
        acc = 0
        for func in self.funcionarios do
            acc += func.salario
        end
        acc
    end

    def num_funcionarios
        self.funcionarios.length
    end

    def add_gerente (funcionario:)
        if self.funcionarios.include?(funcionario) 
            self.gerente = funcionario
        else
            puts "funcionário não pode ser gerente pois não faz parte do departamento!"
        end

        return nil
    end
end