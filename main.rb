require 'faker'
require './departamento.rb'
require './funcionario.rb'
require './dependente.rb'
require './projeto.rb'

# ARRAYS DE FUNCIONARIOS, PROJETOS E DEPTOS

$funcionarios = Array.new
$projetos = Array.new
$departamentos = Array.new

# FUNÇÕES PARA GERAR PARÂMETROS DE INSTANCIAÇÃO DOS OBJETOS

def generate_funcparams
    {:primeiro_nome => Faker::Name.first_name, 
        :segundo_nome => Faker::Name.last_name, 
        :idade => Faker::Number.within(range: 18..80), 
        :salario => Faker::Number.within(range: 1212.00..5000.00).round(2), 
        :endereco => Faker::Address.street_address, 
        :matricula => Faker::Number.number(digits: 3)
    }
end

def generate_dependparams
    {:nome => Faker::Name.name, 
        :idade => Faker::Number.within(range: 18..80), 
        :parentesco => Faker::Relationship.familial
    }
end

def generate_projparams
    {:nome => Faker::App.name,
        :cliente => Faker::Company.name,
        :fundos => Faker::Number.within(range: 10000.00..50000.00).round(2)
    }
end

# 3 DEPARTAMENTOS CRIADOS

world_peace = Departamento.new(nome:"World Peace")
unleash_neo_luddism = Departamento.new(nome:"Unleash Neo-Luddism")
cybersyn = Departamento.new(nome:"Cybersyn")

$departamentos = [world_peace, unleash_neo_luddism, cybersyn]

# PROJETOS

3.times {
    $projetos.push(Projeto.new(generate_projparams))
}

# PREENCHE O ARRAY DE FUNCIONÁRIOS COM UM DEPTO ASSOCIADO E TRES DEPENDENTES

def add_n_funcionarios n
    n.times {

    # PUSH FUNCIONARIO PRA $FUNCIONARIOS

    rand_func = Funcionario.new(generate_funcparams)
    $funcionarios.push(rand_func)

    # ADD FUNCIONARIO NO DEPTO

    rand_depto = $departamentos.sample
    rand_depto.add_funcionario(funcionario: rand_func)

    # ADD 3 DEPENDENTES NO FUNCIONARIO

    3.times {
        rand_func.add_dependente(dependente: Dependente.new(generate_dependparams))
    }

    }
    return nil
end

def print_func_depts func_arr
    func_arr.each {
        |func| puts func.depto.nome
    }
end
