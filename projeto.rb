class Projeto
    attr_accessor :nome, :cliente, :fundos, :depto
    
    def initialize params
        @nome = params[:nome]
        @cliente = params[:cliente]
        @fundos = params[:fundos]
    end

    def add_depto (depto:)
        if self.fundos > depto.calc_gastos
            self.depto = depto
        else
            puts "gastos do departamento são maiores que os fundos do projeto"
        end
        return nil
    end
end